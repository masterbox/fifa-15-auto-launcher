# README #

Don't you ever wonder why EA puts a stupid launcher before you can play Fifa 15 on a PC instead of having a configuration program aside, or even better, inside the game ?
For most player, it's not a big deal, you just need to click a second time.

But, if like me, your PC is in the living room, connected to a gamepad and a remote control, and that you launch your games with Kodi, it's a pain in the a** !!! You need a mouse just for ONE game having a bad design !
 Hopefully, Fifa 15 Auto Launcher is here...

### What is this repository for? ###

It's just a simple script that starts Fifa 15 and automatically click on "Play", so that you don't need a mouse.
This is useful for a console-like PC.

Finally, when the game is over, it will automatically switch to Kodi if it is in the background.


### How do I get set up? ###

* Download [Fifa 15 Auto Launcher.exe](https://bitbucket.org/masterbox/fifa-15-launcher/downloads/fifa%20auto%20launcher.exe) (64 bits version, if somebody needs the 32 bits, ask gently !)
* Put it in your Fifa 15 directory
* Just start this [Fifa 15 Auto Launcher.exe](https://bitbucket.org/masterbox/fifa-15-launcher/downloads/fifa%20auto%20launcher.exe) from your favorite launcher to avoid the pain of clicking on "Play" :)

